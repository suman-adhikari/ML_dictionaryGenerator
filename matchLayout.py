# -*- coding: utf-8 -*-
"""
Created on Tue Dec 05 22:58:17 2017

@author: SUMAN
"""
import csv
from string import digits
import re
import json
import ast

class matchLayout(object):
  
    def readCsv(self):
          
        inputlayout=[]
        with open('E:\\ahack\\HAckthon\\column_names.CSV') as csvfile:
            abc = csv.reader(csvfile)
            for row in abc:
                inputlayout.append(row)        
        return inputlayout     

    def removeEmptycolumn(self,column):
        col=[]
        for row in column:
            row[:] = [item for item in row if item != '']
            col.append(row)
        return col 
    
    def removeUnderscoreHashSpace(self,rows):
        return [[w.replace("_","").replace("#","").replace(" ","").lower() for w in line] for line in rows]
    
    def removeDigits(self,rows):
        for row in rows:
            for item in row:
                #print item
                if item.isdigit():
                    row.remove(item)
        return rows    

    def uniqueColumnName(self,rows):
        return [(list(set(row))) for row in rows]
    
    def removeDigitsFromWords(self,rows):
        return [[ re.sub(r"\d","",item) for item in row]for row in rows]
        
         
    def loadDictionary(self):
        text_file = open("E:\\ahack\\HAckthon\\dict1.py").read()
        data = ast.literal_eval(text_file)       
        return data  
        
    def convertInputAsDictionary(self,inputdata,dictionary):
        newvalue = []
        lastkey = dictionary.keys()[-1]
        for item in inputdata:
            for k, v in dictionary.items():
                if item in v:
                    #print item +": "+ k
                    newvalue.append(k)
                    break   
            
                if k==lastkey:
                    #print item
                    newvalue.append(item)
                    break
        
        return newvalue  
    
    def UniqueColumnAsPerDictionary(self,layout,dictionary):
        layoutAsPerDictionary=[]
        eachrow=[]
        lastkey = dictionary.keys()[-1]
        for i,row in enumerate(layout):
            print i
            for item in row:
                 for k,v in dictionary.items():
                     if item in v:
                         eachrow.append(k)
                         break
                     if k==lastkey:
                         eachrow.append(item)
                         break
            layoutAsPerDictionary.append(eachrow)        
                         
        return layoutAsPerDictionary
        #return [[ [ (re.sub(item,k,item)) for k,v in dictionary.items() if item in v] for item in row]for row in layout]
    
                
                
                
            
        
        
match_layout = matchLayout() 
rows = match_layout.readCsv()
NullRemovedRows = match_layout.removeEmptycolumn(rows)
UnderscoreHashSpaceRemovedRows = match_layout.removeUnderscoreHashSpace(NullRemovedRows)
#print UnderscoreHashSpaceRemovedRows[1]
DigitsRemoved = match_layout.removeDigits(UnderscoreHashSpaceRemovedRows)
DigitsRemovedFromWords = match_layout.removeDigitsFromWords(DigitsRemoved)
UniqueColumn = match_layout.uniqueColumnName(DigitsRemovedFromWords)


dictionary = match_layout.loadDictionary()
UniqueColumnAsDictionary = match_layout.UniqueColumnAsPerDictionary(UniqueColumn,dictionary)
print UniqueColumnAsDictionary

#input = ["memberstartdt","memfirstname","memberenddt","programid","memlastname","memid"]
#input = ["memberstartdt","memfirstname","wtf","memid"]

output = match_layout.convertInputAsDictionary(input,dictionary)


#print "input:" + str(input)
#print "output" + str(output)